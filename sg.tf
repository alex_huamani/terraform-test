resource "aws_security_group" "allow_ssh_anywhere" {
  name        = "test-allow_ssh_anywhere"
  description = "Allow TLS inbound traffic allow_ssh_anywhere"
  #vpc_id      = "${data.aws_vpc.selected.id}"
  vpc_id = "vpc-79a03b01"
  tags = {
    Name = "test-sg-ssh"
  }

  ingress {
    # TLS (change to whatever ports you need)
    from_port = 22
    to_port   = 22
    protocol  = "tcp"
    # Please restrict your ingress to only necessary IPs and ports.
    # Opening to 0.0.0.0/0 can lead to security vulnerabilities.
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
